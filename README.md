# IRCBot

This is an IRC bot created using [Twisted](http://twistedmatrix.com/trac/). The bot features two different kinds of interaction: **commands** and **parsers**.

## Required Modules
-----
The IRC bot requires Twisted to run. Just use pip to install: `pip install twisted`
If you are using Windows, then I do not have any help nor sympathy to give. Good luck!

## Using the Bot
-----
To run the bot, simply run `run.py` as an executable or use `python run.py`. You can also specify options with `run.py`, such as including a **host:port** and a **chan**.
If you try to run the bot out of the box, you will see the following message:

```
$ python run.py 

WARNING: You may want to create a settings file in the
root directory of this bot, or bad things will happen.
```

The bot will then fail to connect because the connection information are defaulted to empty strings. The port defaults to 6667.

## Settings
-----
It is **VERY HIGHLY RECOMMENDED** that you include a `settings.py` file in the root directory of the bot. If you do not, you will see a warning message and bad things will happen. Currently, the settings file requires the following fields:

+ IRC_CONNECT_HOST : host of the IRC server (default '')
+ IRC_CONNECT_PORT : port of the IRC server (default 6667)
+ IRC_DEFAULT_USER : nickname to use when connecting to the IRC server (default '')
+ IRC_DEFAULT_CHAN : the name of the channel to connect to (default '')

The following fields are optional:

+ IRC_CONNECT_PASS : password for the IRC server (default '')

If you need to store any other constants or information, you can put them in this file to be accessed by other modules.

### Accessing Settings in Code
If you need to refer to your settings in code (when adding new comments or parsers), use the following import statement:
`from conf import settings`

## Commands
-----
The bot recognizes commands when users say anything with the first character as an exclamation mark:

```
user: !help

bot: Available commands: !urban !help !listparsers !tweets !define
bot: Use !help with a valid command to see its documentation.
```

### Adding Commands
To add your own command functionality to the bot:

1. Add your command method to an existing file in `commands/` or in a new file in the same directory.
2. If you create a new module, add the following import statement to the `router.py` file in `commands/`: `from [command module name] import *`

### Defining Commands
Each command method is defined like the following structure:

```
def command_name(*args, **kwargs):
    '''
    Docstring to be picked up by the 'help' command
    '''
    _handle the command_
    return a dictionary
```

A dictionary must be return with one of the keys `error` or `result`. If both are included, `error` will be read and `result` will be ignored.

If you have any helper functions defined in your commands module and you do not want them to be recognized by the bot, add a docstring to the function with the string `NO_RECORD`.

## Parsers
-----
Parsers are called every time a user says anything. For example, the YouTube parser detects YouTube links and responds with some information:

```
user: http://www.youtube.com/watch?v=9bZkp7q19f0

bot: YouTube Title: PSY - GANGNAM STYLE (강남스타일) M/V
bot: Views: 1223933787 Length: 4:13
```

### Adding Parsers
To add your own parser to the bot, simply add your own module to `parsers/`. It will automatically be picked up when the bot is initiated.

### Defining Parsers
Parsers can include any sort of code, but it must define a method called `parse` that takes in a parameter for the message string and return a dictionary with at least one of the keys `error` or `result`. To provide documentation for the parser, add a module-level docstring (a docstring at the top of the file).