'''
Command handler
'''
import re
import router
import parsers
from inspect import getmembers, isfunction
from twitter import *
from dicts import *

# TODO: restructure router so plugins don't have to be imported here

def execcmd(cmd):
    assert cmd[0] == '!'
    args = filter(lambda x: bool(x.strip()), cmd[1:].split(' '))
    cmd = args.pop(0)
    kwargs = {}
    for arg in args:
        match = re.search(r'^(\w+)\s*=\s*(\w+)$', arg)
        if match: kwargs[match.group(1)] = match.group(2)
    try:
        return getattr(router, cmd)(*args, **kwargs)
    except AttributeError:
        return { 'error': 'I am sorry, but I could not recognize the command "%s".' % cmd }

def help(*args, **kwargs):
    '''
    !help [commands...]
    Display available commands or
    docstrings on specified commands
    '''
    if args:
        help_str = ''
        for arg in args:
            try:
                docstr = getattr(router, arg).__doc__
                if not docstr: docstr = 'No documentation for this command'
            except AttributeError:
                docstr = 'Command not recognized'
            help_str += '%s: %s\n' % (arg, docstr)
        return { 'result': '%s' % help_str }
    else:
        cmd_exclude = ['getmembers', 'isfunction', 'execcmd']
        valid_cmds = dict(filter(lambda x: x[0] not in cmd_exclude and not re.search(r'NO_RECORD', x[1].__doc__ or ''),
            getmembers(router, isfunction)))
        cmd_str = ''
        for cmd in valid_cmds.keys():
            cmd_str += '!%s ' % cmd
        return { 'result': 'Available commands: %s\nUse !help with a valid command to see its documentation.' % cmd_str }

def listparsers(*args, **kwargs):
    '''
    !parsers [parsers...]
    Lists all active parsers
    If parsers are specified, then its documentation is printed
      - parsers: argument list of parser names
    '''
    if args:
        help_str = ''
        for arg in args:
            try:
                docstr = getattr(router.parsers, arg).__doc__
                if not docstr: docstr = 'No documentation for this command'
            except AttributeError: docstr = 'Parser name not recognized'
            help_str += '%s: %s\n' % (arg, docstr)
        return { 'result': '%s' % help_str }
    else:
        psr_str = ''
        for psr in [mod.__name__.split('.')[-1] for mod in parsers.parsers]: psr_str += '- %s\n' % psr
        return { 'result': 'Active parsers:\n%s' % psr_str }