'''
Commands that use the Twitter API
'''

import urllib
import json

def tweets(*args, **kwargs):
    '''
    !tweets screenname [count]
    Gets a user's most recent tweets
      - screenname: the user's twitter handle
      - count: a number in the range [1, 20]
    '''
    sn = None
    count = 5
    if len(args) > 0: sn = args[0]
    if len(args) > 1: count = int(args[1])
    sn = kwargs.get('screenname', sn)
    if not sn: return { 'error': 'A Twitter screenname must be specified' }
    count = int(kwargs.get('count', count))
    if count < 1 or count > 20: return { 'error': 'Count must be a number in [0, 20]' }
    args = { 'screen_name': sn, 'count': count }
    endpoint = 'http://api.twitter.com/1/statuses/user_timeline.json?'
    feed = json.load(urllib.urlopen('%s%s' % (endpoint, urllib.urlencode(args))))
    feed_str = ('%s\'s most recent tweets:\n' % sn).upper()
    for argi, entry in enumerate(feed):
        feed_str += '%d. %s\n' % (argi + 1, entry['text'])
    return { 'result': str(feed_str) }
    