import global_settings

class Settings(object):
    def __init__(self):
        self.load_settings(global_settings)
        try:
            import settings
            self.load_settings(settings)
        except ImportError:
            print 'WARNING: You may want to create a settings file in the\n' + \
                'root directory of this bot, or bad things will happen.\n'

    def load_settings(self, settings_module):
        for setting in dir(settings_module):
            if setting == setting.upper():
                setattr(self, setting, getattr(settings_module, setting))

settings = Settings()