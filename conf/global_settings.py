'''
Default settings module.
Users are HIGHLY RECOMMENDED to place their own
settings module in the root directory of the bot.
'''

IRC_CONNECT_HOST = '127.0.0.1'
IRC_CONNECT_PORT = 6667
IRC_CONNECT_PASS = ''
IRC_DEFAULT_USER = ''
IRC_DEFAULT_CHAN = ''
IRC_JOIN_MESSAGE = 'Hello everyone! Say "!help" to learn more about how I can be of service.'