'''
Class definitions for the IRCBot
'''
import sys
import commands
import parsers
from conf import settings
from twisted.words.protocols import irc
from twisted.internet import protocol, reactor

class ChatBot(irc.IRCClient):
    def _get_nickname(self):
        return self.factory.nickname
    nickname = property(_get_nickname)

    def signedOn(self):
        self.join(self.factory.channel)
        print 'Signed on as %s.' % (self.nickname)

    def joined(self, channel):
        print 'Joined %s' % (channel)
        self.say(channel, settings.IRC_JOIN_MESSAGE)

    def privmsg(self, user, channel, msg):
        if msg and msg[0] == '!':
            payload = commands.execcmd(msg)
            if 'error' in payload: self.say(channel, payload['error'])
            else: self.say(channel, payload['result'])
        else:
            payload = parsers.parse(msg)
            if 'error' in payload: self.say(channel, payload['error'])
            elif payload['responses']:
                for response in payload['responses']: self.say(channel, response)

class ChatBotFactory(protocol.ClientFactory):
    protocol = ChatBot

    def __init__(self, channel, nickname=settings.IRC_DEFAULT_USER, password=None):
        self.channel = channel
        self.nickname = nickname
        self.protocol.password = password

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection (%s), reconnecting...' % (reason)
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print 'Could not connect: %s' % (reason)