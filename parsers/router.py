'''
Parser handler
'''

import os
import re

modules = set([m.split('.')[0] for m in os.listdir(os.path.dirname(__file__)) if re.match(r'^.*\.py$', m)])
excludes = set(['__init__', 'router'])
parsers = []
for parser in modules - excludes:
    try:
        mod = __import__('parsers.%s' % parser)
        parsers.append(getattr(mod, parser))
    except ImportError: continue

def parse(utterance):
    data = { 'responses': [] }
    for parser in parsers:
        response = parser.parse(utterance)
        if response: data['responses'].append(response)
    return data