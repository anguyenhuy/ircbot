'''
    Detects YouTube links and responds with
    the title, duration, and number of views.
'''

import re
import gdata.youtube.service

def get_yt_info(yt_id):
    yts = gdata.youtube.service.YouTubeService()
    uri = 'http://gdata.youtube.com/feeds/api/videos/%s?v=2' % yt_id
    try:
        entry = yts.GetYouTubeVideoEntry(uri)
    except gdata.service.RequestError:
        return { 'error': 'ERROR: Could not retrieve video information for ID %s.' % yt_id }
    length = int(entry.media.duration.seconds)
    data = {
        'title': entry.title.text,
        'duration': '%d:%02d' % (length / 60, length % 60),
        'views': entry.statistics.view_count,
    }
    return data

def parse(utterance):
    match = \
re.search(r'http(?:s?)://(?:www\.)?youtu(?:be\.com/watch\?|\.be/)(?:[\w]+=[\w]+&)*(?:v=([\w\-]+))(&(amp;)?[\w\?=]*)?',
        utterance)
    if match:
        data = get_yt_info(match.group(1))
        return 'YouTube Title: %s\nViews: %s Length: %s' % (data['title'], data['views'], data['duration'])
    return ''