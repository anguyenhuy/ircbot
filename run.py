#!/usr/bin/env python
'''
Entry point for running the bot
'''

import sys
import os
import getopt
import getpass
import ircbot
from conf import settings
from twisted.internet import reactor

sys.path.append(os.path.abspath(__file__))

if __name__ == "__main__":
    host = settings.IRC_CONNECT_HOST
    port = settings.IRC_CONNECT_PORT
    pswd = settings.IRC_CONNECT_PASS
    chan = settings.IRC_DEFAULT_CHAN
    name = settings.IRC_DEFAULT_USER
    prompt_pass = False
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'c:H:n:p', ['chan', 'host', 'name'])
    except getopt.GetoptError as err:
        sys.stderr.write(str(err))
        sys.exit(1)
    for opt, arg in opts:
        if opt in ('-c', '--chan'): chan = arg
        elif opt in ('-H', '--host'):
            parts = arg.split(':')
            if len(parts) > 0: host = parts[0]
            if len(parts) > 1: port = int(parts[1])
        elif opt in ('-n', '--name'): name = arg
        elif opt in ('-p'): prompt_pass = True
    if prompt_pass: pswd = getpass.getpass()
    reactor.connectTCP(host, port, ircbot.ChatBotFactory('#%s' % chan, password=pswd))
    reactor.run()

# TODO: help text
